function repetedData(deliveries, matches, year) {
  var matchId = matches
    .filter(matchYear => {
      return matchYear["season"] == year;
    })
    .map(ids => ids["id"]);
  var filterdDeliveries = deliveries.filter(deliveryId =>
    matchId.includes(deliveryId["match_id"])
  );
  return filterdDeliveries;
}
module.exports = {
  matchesPerSeason: function(matches) {
    return matches.reduce((matchesPerYear, match) => {
      if (matchesPerYear[match["season"]] == undefined) {
        matchesPerYear[match["season"]] = 1;
      } else {
        matchesPerYear[match["season"]]++;
      }
      return matchesPerYear;
    }, {});
  },
  //console.log(matchesPerSeason(matches));
  noOfMatchesWonPerTeamPerYear: function(matches) {
    return matches.reduce((matchesWonPerYear, match) => {
      if (match["winner"] != "") {
        if (matchesWonPerYear[match["winner"]] == undefined) {
          matchesWonPerYear[match["winner"]] = {};
          matchesWonPerYear[match["winner"]][match["season"]] = 1;
        } else {
          if (
            matchesWonPerYear[match["winner"]][match["season"]] == undefined
          ) {
            matchesWonPerYear[match["winner"]][match["season"]] = 1;
          } else {
            matchesWonPerYear[match["winner"]][match["season"]]++;
          }
        }
      }
      return matchesWonPerYear;
    }, {});
  },
  //console.log(noOfMatchesWonPerTeamPerYear(matches));
  extraRunsGivenByTeam: function(deliveries, matches) {
    var filterdDeliveriesData = repetedData(deliveries, matches, 2016);
    return filterdDeliveriesData.reduce((extraRuns, delevriecurrIndex) => {
      if (extraRuns[delevriecurrIndex["bowling_team"]] == undefined) {
        extraRuns[delevriecurrIndex["bowling_team"]] = parseInt(
          delevriecurrIndex["extra_runs"]
        );
      } else {
        extraRuns[delevriecurrIndex["bowling_team"]] += parseInt(
          delevriecurrIndex["extra_runs"]
        );
      }
      return extraRuns;
    }, {});
  },
  //console.log(extraRunsGivenByTeam());
  topTenEconomicBowler: function(deliveries, matches) {
    var filterdDeliveriesData = repetedData(deliveries, matches, 2015);
    var totalBowlersEconomy = filterdDeliveriesData.reduce(
      (bowlersEconomy, deliverie) => {
        if (bowlersEconomy[deliverie["bowler"]] == undefined) {
          bowlersEconomy[deliverie["bowler"]] = {};
          bowlersEconomy[deliverie["bowler"]]["runs"] =
            parseInt(deliverie["wide_runs"]) +
            parseInt(deliverie["noball_runs"]) +
            parseInt(deliverie["batsman_runs"]);
          bowlersEconomy[deliverie["bowler"]]["balls"] = 1;
          bowlersEconomy[deliverie["bowler"]]["economy"] =
            (bowlersEconomy[deliverie["bowler"]]["runs"] /
              bowlersEconomy[deliverie["bowler"]]["balls"]) *
            6;
        } else {
          bowlersEconomy[deliverie["bowler"]]["runs"] +=
            parseInt(deliverie["wide_runs"]) +
            parseInt(deliverie["noball_runs"]) +
            parseInt(deliverie["batsman_runs"]);
          bowlersEconomy[deliverie["bowler"]]["balls"]++;
          bowlersEconomy[deliverie["bowler"]]["economy"] =
            (bowlersEconomy[deliverie["bowler"]]["runs"] /
              bowlersEconomy[deliverie["bowler"]]["balls"]) *
            6;
        }
        return bowlersEconomy;
      },
      {}
    );
    return Object.entries(totalBowlersEconomy)
      .sort((a, b) => {
        return a[1]["economy"] - b[1]["economy"];
      })
      .slice(0, 10)
      .reduce((topTenBowlers, b) => {
        topTenBowlers[b[0]] = b[1]["economy"];
        return topTenBowlers;
      }, {});
  }
  //console.log(topTenEconomicBowler());
};
