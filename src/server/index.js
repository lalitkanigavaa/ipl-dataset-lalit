var fs = require("fs");
var ipl = require("./ipl.js");
const matches = JSON.parse(fs.readFileSync("../data/matches.json", "utf-8"));
const deliveries = JSON.parse(
  fs.readFileSync("../data/deliveries.json", "utf-8")
);
var matchesPlayedPerYear = ipl.matchesPerSeason(matches);
var matchesWonPerTeam = ipl.noOfMatchesWonPerTeamPerYear(matches);
var extraRunsCoceded = ipl.extraRunsGivenByTeam(deliveries, matches);
var topTenBowler = ipl.topTenEconomicBowler(deliveries, matches);
var output1 = JSON.stringify(matchesPlayedPerYear, null, 2);
var output2 = JSON.stringify(matchesWonPerTeam, null, 2);
var output3 = JSON.stringify(extraRunsCoceded, null, 2);
var output4 = JSON.stringify(topTenBowler, null, 2);
fs.writeFileSync("../output/matchesPlayedPerYear.json", output1);
fs.writeFileSync("../output/matchesWonPerTeam.json", output2);
fs.writeFileSync("../output/extraRunsCoceded.json", output3);
fs.writeFileSync("../output/topTenBowler.json", output4);
